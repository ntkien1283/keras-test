import ipdb
import numpy as np
from keras import backend
from keras.layers import Embedding, Multiply, Subtract, Input, Lambda, Activation, merge, Flatten
from keras.regularizers import l2
from keras.models import Model
from keras.optimizers import Adam
import data
from sklearn.metrics import roc_auc_score

def identity_loss(y_true, y_pred):
    return y_pred

def build_model(num_users, num_items, mf_dim):
    user_input = Input(shape=(1,), name='user_input')
    item_i_input = Input(shape=(1,), name='item_i_input')
    item_j_input = Input(shape=(1,), name='item_j_input')
    user_embedding = Embedding(input_dim = num_users, output_dim = mf_dim, name = 'user_embedding', input_length=1)
    item_embedding = Embedding(input_dim = num_items, output_dim = mf_dim, name = 'item_embedding', input_length=1)
    user_vector = user_embedding(user_input)
    item_i_vector = item_embedding(item_i_input)
    item_j_vector = item_embedding(item_j_input)
    user = Flatten(name='user_vector_flat')(user_vector) 
    itemi= Flatten(name='itemi_vector_flat')(item_i_vector)
    itemj = Flatten(name='itemj_vector_flat')(item_j_vector)
    Xui = merge([user, itemi], mode='dot', name='Xui')
    Xuj = merge([user, itemj], mode='dot', name='Xuj')
    Xuij = Subtract(name='Xuij')([Xui, Xuj])
    sig = Activation('sigmoid', name='sigmoid')(Xuij)
    bpr_loss = Lambda(lambda x: 1-x, name='bpr')(sig)
    model = Model(input=[user_input, item_i_input, item_j_input], output=bpr_loss)
    model.compile(loss=identity_loss, optimizer=Adam())
    return model

def predict(trained_model, user_id, item_ids):
    user_vector = trained_model.get_layer('user_embedding').get_weights()[0][user_id]
    item_vector_mat = trained_model.get_layer('item_embedding').get_weights()[0][item_ids]
    scores = (np.dot(user_vector, item_vector_mat.T))
    return scores

def auc(model, test_data):
    """
        Measure AUC for model and ground truth on all items.
        Returns:
        - float AUC
    """

    test_data = test_data.tocsr()

    no_users, no_items = test_data.shape

    pid_array = np.arange(no_items, dtype=np.int32)

    scores = []
    for user_id, row in enumerate(test_data):
        predictions = predict(model, user_id, pid_array)
        true_pids = row.indices[row.data == 1]
        grnd = np.zeros(no_items, dtype=np.int32)
        grnd[true_pids] = 1
        if len(true_pids):
            scores.append(roc_auc_score(grnd, predictions))

    return sum(scores) / len(scores)
if __name__ == '__main__':
    mf_dim = 50
    n_epochs = 10
    # Read data
    train, test = data.get_movielens_data()
    num_users, num_items = train.shape
    model = build_model(num_users, num_items, mf_dim) 
    print(model.summary())
    for e in range(n_epochs):
        uid, pid, nid = data.get_triplets(train)
        model.fit([uid, pid, nid], [1]*len(uid), batch_size=64, nb_epoch=1)
        print(auc(model, test))
